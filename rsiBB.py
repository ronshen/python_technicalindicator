import talib as tb
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd



SPX = pd.read_csv("SPX.csv")

def getBBands(df, period = 20, stdev = 2):
    close = df['Close']
    upper, mid, lower = tb.BBANDS(close, timeperiod = period, nbdevup = stdev, nbdevdn = stdev, matype = 0)
    data = dict(upper = upper, middle = mid, lower = lower)
    df = pd.DataFrame(data, index=df.index, columns=['upper', 'middle', 'lower'])
    return df



bb = getBBands(SPX)
rsi = tb.RSI(SPX["Close"], timeperiod = 14)

#plot graph
candlestick = go.Candlestick(x = SPX['Dates'],
                              open = SPX['Open'],
                              high = SPX['High'],
                              low = SPX['Low'],
                              close = SPX['Close'])

bbmiddle =  go.Scatter(x = SPX['Dates'], y = bb['middle'],name="BB Middle Band")
bbupper = go.Scatter(x = SPX['Dates'], y = bb['upper'],name="BB Upper Band")
bblower = go.Scatter(x = SPX['Dates'], y = bb['lower'],name="BB Lower Band")
rsichart = go.Scatter(x = SPX['Dates'], y = rsi,name="RSI")
        
                              
fig = make_subplots(rows=2,cols=1,shared_xaxes=True,vertical_spacing=0.02,
                    specs=[[{"secondary_y": True}],
                           [{"secondary_y": True}]])
                           
                                          
fig.add_trace(candlestick,secondary_y=False,row=1,col=1)
fig.add_trace(bbmiddle,row=1,col=1,secondary_y=True)
fig.add_trace(bbupper,row=1,col=1,secondary_y=True)
fig.add_trace(bblower,row=1,col=1,secondary_y=True)
fig.add_trace(rsichart,row=2,col=1,secondary_y=False)


# fig.update_xaxes(
#     rangebreaks=[
#         dict(bounds=["sat", "mon"]), #hide weekends
#     ]
# )

fig.update_layout(xaxis_rangeslider_visible=False)

fig.show()
